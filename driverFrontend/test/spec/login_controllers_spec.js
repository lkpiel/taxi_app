describe('Controllers', function(){
    var scope, $httpBackend, cookies, location;

    // load the controller's module
    beforeEach(module('starter.controllers'));
    beforeEach(inject(function($rootScope, $controller, _$httpBackend_, $cookies,  $location) {
        $httpBackend = _$httpBackend_;
        cookies = $cookies
        scope = $rootScope.$new();
        location = $location;
        $controller('LoginCtrl', {$scope: scope});
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    // tests start here
    it('Create cookies and message after correct submit', function(){
        spyOn(location, 'path');
        scope.username = "taxi1"
        scope.password = "parool1"
        $httpBackend
          .expectPOST('http://localhost:3000/login',
              {username: scope.username, password: scope.password, type: "taxi"})
          .respond(201, {authenticated: true,  authKey: "1234", username: "taxi1", type:"taxi"});
        scope.doLogin();
        $httpBackend.flush();
        expect(scope.message).toEqual("");
        expect(cookies.get('username')).toEqual('taxi1');
        expect(cookies.get('key')).toEqual('1234');
        expect(location.path).toHaveBeenCalledWith('/orders');

    });

    it('Removes cookies and right message after correct submit', function(){
        scope.username = "taxi1"
        scope.password = "31293812"
        $httpBackend
          .expectPOST('http://localhost:3000/login',
              {username: scope.username, password: scope.password, type: "taxi"})
          .respond(201, {authenticated: false});
        scope.doLogin();
        $httpBackend.flush();
        expect(scope.message).toEqual("Invalid username or password");
        expect(cookies.get('username')).toEqual(undefined);
        expect(cookies.get('key')).toEqual(undefined);

    });
    it('Removes cookies and redirects after correct log out', function(){
        spyOn(location, 'path');
        scope.username = "taxi1"
        scope.password = "parool1"
        $httpBackend
          .expectPOST('http://localhost:3000/login',
              {username: scope.username, password: scope.password, type: "taxi"})
          .respond(201, {authenticated: true,  authKey: "1234", username: "taxi1", type:"taxi"});
        scope.doLogin();
        $httpBackend.flush();

        $httpBackend
          .expectPOST('http://localhost:3000/logout',
              {username: scope.username, key: cookies.get('key'), type: "taxi"})
          .respond(201, {authenticated: false});
        scope.doLogout();
        $httpBackend.flush();
        expect(cookies.get('username')).toEqual(undefined);
        expect(cookies.get('key')).toEqual(undefined);
        expect(location.path).toHaveBeenCalledWith('/login');
    });
    it('Doesn\'t remove cookies and alerts after unsuccesful log out', function(){
        spyOn(window, 'alert');
        scope.username = "taxi1"
        scope.password = "parool1"
        $httpBackend
          .expectPOST('http://localhost:3000/login',
              {username: scope.username, password: scope.password, type: "taxi"})
          .respond(201, {authenticated: true,  authKey: "1234", username: "taxi1", type:"taxi"});
        scope.doLogin();
        $httpBackend.flush();

        $httpBackend
          .expectPOST('http://localhost:3000/logout',
              {username: scope.username, key: "1234", type: "taxi"})
          .respond(201, {authenticated: true});
        scope.doLogout();
        $httpBackend.flush();
        expect(cookies.get('username')).toEqual("taxi1");
        expect(cookies.get('key')).toEqual("1234");
        expect(window.alert).toHaveBeenCalledWith('Unsuccesful logout. Please contact support if problems continue.');


    });
});
