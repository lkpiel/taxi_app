var app = angular.module('starter', ['ionic', 'starter.controllers', 'ngCookies']);

app.run(function($ionicPlatform, $rootScope, $cookies, $http, $window) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    $rootScope.$on( '$stateChangeStart', function(e, toState  , toParams , fromState, fromParams) {
      $http.post('http://localhost:3000/checkAuth', {username: $cookies.get("username"), key: $cookies.get("key"), type:"taxi"})
        .then(function (response) {
            if (response.data.authenticated == false){
              $window.location.href  = "./#/login"
            }
        });
    });
    $http.post('http://localhost:3000/checkAuth', {username: $cookies.get("username"), key: $cookies.get("key"), type:"taxi"})
      .then(function (response) {
          if (response.data.authenticated == false)
            $window.location.href  = "./#/login"
    });
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('orders', {
      url: '/orders',
      abstract:true,
      templateUrl: 'templates/menu.html',
      controller: 'OrdersCtrl'
    })
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })
    .state('orders.view', {
      url: '',
      views: {
        templateURL: 'templates/menu.html',
        'menuContent': {
          templateUrl: 'templates/orders.html'
        }
      },
      controller: 'OrdersCtrl'
    });
    $urlRouterProvider.otherwise('/login');
});
