'use strict';

var app = angular.module('starter');

app.service('PusherService', function ($rootScope, $cookies) {
  var pusher = new Pusher('9a112866bc5824b24c9c');
  var channel = pusher.subscribe($cookies.get("username"));
  return {
    onMessage: function (callback) {
      channel.bind('rocky_road', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});
