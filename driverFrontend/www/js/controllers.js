var app = angular.module('starter.controllers', ['ngCookies']);

app.controller('OrdersCtrl', function ($scope, $ionicModal, $http, PusherService, $cookies) {
  $scope.message = '';
  $scope.client_name = '';
  $scope.client_number = '';
  $scope.client_pickup = '';
  $scope.price =  0.0;
  PusherService.onMessage(function (response) {
    $('.waitingMessage').html("&nbsp&nbsp&nbspWAITING FOR ORDERS...");

    if(response.message == "client_cancelled_request"){
      $http.post('http://localhost:3000/bookings/make_available', {taxi: $cookies.get('username')})
        .then(function (response) {
          console.log("Made driver available!");
      });
      $('.startRide').css("display", "inline-block");
      $('.cancelRide').css("display", "inline-block");

      $('.rideControls').css("display", "none");
      $('.finishRide').css("display", "none");
      $('.waitingMessage').html("<span style = 'color:red'>Client cancelled your ride</span><br>&nbsp&nbsp&nbspWAITING FOR ORDERS...");
      $('.waitingMessage').css("display", "block");
      $('.driverMessage').html("Your client is waiting for you!");
    } else {
      console.log("Client " + response.client_name);
      $scope.message = response.message;
      $scope.client_name = response.client_name;
      $scope.client_number = response.client_number;
      $scope.client_pickup = response.client_pickup;
      $scope.client_username = response.client_username;
      $('.waitingMessage').css("display", "none");
      $('.driverMessage2').css("display", "block");
      //$scope.modal.show();
    }
  });

  $ionicModal.fromTemplateUrl('templates/bookings/async-notification.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
  });

  $scope.closeModal = function () {
  /*  $scope.message = '';
    $scope.client_name = '';
    $scope.client_number = '';
    $scope.client_pickup = '';*/
    $scope.modal.hide();
  };

  // $scope.acceptMission = function () {
  //   console.log("I'M ON A MISSION!");
  //   // TODO: change booking status to accepted (?), change driver's status to busy and show him map?
  //   $http.post('http://localhost:3000/bookings/decision', {
  //     taxi: $cookies.get('username'),
  //     decision: 'accept'
  //   })
  //     .then(function(response) {
  //
  //     });
  //   $scope.closeModal();
  // };
  //
  // $scope.rejectMission = function () {
  //   console.log("I have better things to do..");
  //   // TODO: send information about new booking to the next closest (and longest time available) driver
  //   $http.post('http://localhost:3000/bookings/decision', {
  //     taxi: $cookies.get('username'),
  //     decision: 'decline'
  //   });
  //   $scope.closeModal();
  // };

  $scope.acceptMission = function () {
    console.log("I'M ON A MISSION!");
    // TODO: change booking status to accepted (?), change driver's status to busy and show him map?
    $http.post('http://localhost:3000/bookings/decision', {username: $cookies.get('username'), decision: 'accept', client_username: $scope.client_username, client_address: $scope.client_pickup})
    $scope.closeModal();
    $http.post('http://localhost:3000/bookings/make_busy', {taxi: $cookies.get('username')})
      .then(function (response) {

        $scope.price =0.0;
        $('.rideSum').html(($scope.price).toFixed(1));
        $('.waitingMessage').css("display", "none");
        $('.finishRide').css("display", "none");
        $('.driverMessage2').css("display", "none");
        $('.endRide').css("display", "none");
        $('.rideControls').css("display", "block");
        console.log("Made driver busy");
      });


  };

  $scope.rejectMission = function () {
    console.log("I have better things to do..");
    // TODO: send information about new booking to the next closest (and longest time available) driver
    $http.post('http://localhost:3000/bookings/decision', {username: $cookies.get('username'), decision: 'decline', client_username: $scope.client_username})
    $scope.closeModal();
    $('.driverMessage2').css("display", "none");
    $('.waitingMessage').css("display", "block");
  };
  $scope.startRide = function(){
    $('.startRide').css("display", "none");
    $('.cancelRide').css("display", "none");
    $('.endRide').css("display", "inline-block");
    $('.driverMessage').html("Drive safe!");
    $scope.price = 0.0;
    $scope.myInterval = setInterval(function () {
        $scope.price =$scope.price +0.1;
        $('.rideSum').html(($scope.price).toFixed(1));
    }, 1000);
    $http.post('http://localhost:3000/bookings/notify_client_that_ride_started', {client_username: $scope.client_username})
      .then(function (response) {

    });

  }
  $scope.endRide = function(){
    $('.finishRide').css('display', 'inline-block');
    $('.cancelRide').css("display", "none");

    $('.endRide').css("display", "none");
    $('.driverMessage').html("Have a nice day!");
    $http.post('http://localhost:3000/bookings/notify_client_that_ride_ended', {client_username: $scope.client_username, price: $scope.price})
      .then(function (response) {
        console.log("Sent end ride to client");
        console.log($scope.price);
    });
    $http.post('http://localhost:3000/bookings/make_available', {taxi: $cookies.get('username')})
      .then(function (response) {
        console.log("Made driver available!");
    });
    $scope.price = 0.0;
    clearInterval($scope.myInterval);
  }
  $scope.cancelRide = function(){
    $('.waitingMessage').css("display", "block");
    $('.rideControls').css("display", "none");
    $('.driverMessage').html("Your client is waiting for you!");
    $http.post('http://localhost:3000/bookings/make_available', {taxi: $cookies.get('username')})
      .then(function (response) {
        console.log("Made driver available!");
      });
      clearInterval($scope.myInterval);
  }
  $scope.finishRide = function(){
    $('.startRide').css("display", "inline-block");
    $('.cancelRide').css("display", "inline-block");

    $('.rideControls').css("display", "none");
    $('.finishRide').css("display", "none");

    $('.waitingMessage').css("display", "block");
    $('.driverMessage').html("Your client is waiting for you!");
  }

  $scope.submit = function () {
  }
});


app.controller('LoginCtrl', function ($location, $scope, $http, $cookies) {
  $scope.doLogin = function () {
      $http.post('http://localhost:3000/login', {
      username: $scope.username,
      password: $scope.password,
      type: "taxi"
    })
      .then(function (response) {
        if (response.data.authenticated == true && response.data.type == "taxi") {
          $http.post('http://localhost:3000/bookings/make_available', {taxi:   $scope.username})
            .then(function (response) {
              console.log("Made driver available!");
            });
            $cookies.put("username", response.data.username)
            $cookies.put("key", response.data.authKey)
            $scope.message = "";
            $location.path('/orders');

        }
        else {
          $cookies.remove("username")
          $cookies.remove("key")
          $scope.message = "Invalid username or password"
        }
      });
  };
  $scope.doLogout = function () {
    $http.post('http://localhost:3000/logout', {
      username: $cookies.get("username"),
      key: $cookies.get("key"),
      type: "taxi"
    })
      .then(function (response) {
        if (response.data.authenticated == false) {
          $http.post('http://localhost:3000/bookings/make_invisible', {taxi:  $cookies.get('username')})
            .then(function (response) {
              $cookies.remove("username");
              $cookies.remove("key");
              $location.path('/login');
            });
        }
        else
          {
            alert("Unsuccessful logout. Please contact support if problems continue.");
          }
        });
  }
});
