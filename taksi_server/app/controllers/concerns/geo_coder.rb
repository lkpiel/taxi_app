require 'time' # for calculating who has been available the longest


module GeoCoder

  GOOGLE_API_KEY = 'AIzaSyCID2CC9UnwIR9sQJ4XB-RvSqTCbUXwir8'

  def self.get_address(latitude, longitude)
    parse_uri = URI.encode("http://maps.googleapis.com/maps/api/geocode/json?latlng=#{latitude},#{longitude}")
    parsed_uri = URI.parse(parse_uri)
    #Rails.logger.debug parsed_uri
    response = Net::HTTP.get(parsed_uri)
    JSON.parse(response)
  end

  ## imo working
  def self.get_directions(origin, destination)
    parse_uri = URI.encode("http://maps.googleapis.com/maps/api/directions/json?origin=#{origin}&destination=#{destination}")
    parsed_url = URI.parse(parse_uri)
    #Rails.logger.debug parsed_url
    response = Net::HTTP.get(parsed_url)
    sleep(1.0/10)
    JSON.parse(response)
  end

  ## imo working
  def self.get_distance(origin, destination)
    result = get_directions(origin, destination)
    #puts "ASDASPDAÖSLKDJALÖSKDJA LSKDJNAPSODIUAPNOSIDUNAPOSIDUPAOSIUDASD"
    puts result['status']
    if result['status'] == 'OK'
      distance = result["routes"].first["legs"].first["distance"]["value"]
    end
    #puts "JAA JUJMALA DISTANCE: " + distance.to_s
    distance
  end

  ## imo working
  def self.get_closest_taxi_array(inserted_address)
    available_taxis = Taxi.where(status: 'available').to_a
    Rails.logger.debug "OLEEEEEN KÄNGGSDER" + available_taxis.size.to_s   + "\n"
    #Rails.logger.debug available_taxis[0].available_since.to_s
    #Rails.logger.debug available_taxis[1].available_since.to_s
    puts "TERETERE    " + available_taxis.size.to_s + "\n"
    #st=0; ActiveRecord::Base.send(:subclasses).each {|sc| st += sc.all.size.to_s};   Rails.logger.debug  "Total number of rows is+ " + st.to_s
    current_time = Time.new
    sorted_by_dist = available_taxis.sort! {|driver1, driver2|
      distance1 = get_distance(inserted_address, driver1.address).to_i
      distance2 = get_distance(inserted_address, driver2.address).to_i
      [distance1] <=> [distance2]
    }.sort! {|driver1, driver2|
      time1 = (driver1.available_since - current_time)
      time2 = (driver2.available_since - current_time)
      [time1] <=> [time2]
    }

    #puts "JAAJUMALA SORTED: " + sorted_by_dist.to_s
    #Rails.logger.debug offered_taxi.available_since.to_s
    sorted_by_dist
  end



  def self.get_taxi_eta(closest_taxi, inserted_address)
    directions = get_directions(closest_taxi.address, inserted_address)
    #Rails.logger.debug directions["routes"].first
    eta = directions["routes"].first["legs"].first["duration"]["text"]
    #Rails.logger.debug eta
    eta
  end


end
