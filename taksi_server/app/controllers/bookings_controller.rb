

class BookingsController < ApplicationController

  include GeoCoder

  def create
    # gets the params and replies automatically
    if params[:address] && params[:address] != ""
      puts "MA SEAL EI JOU"
      inserted_address = params[:address]
      puts "INSERTED: " + inserted_address.to_s
      closest_taxis_array = GeoCoder.get_closest_taxi_array(inserted_address)
      puts "CLOSEST TAXIS: " + closest_taxis_array.to_a.to_s
      closest_taxi_eta = GeoCoder.get_taxi_eta(closest_taxis_array[0], inserted_address)
      #taxi_message = closest_taxis_array[0].username + ' is coming for you, ETA: ' + closest_taxi_eta
      puts 'OASIDJAOIJ OOOJJJEEEEEEEEE   ' + closest_taxis_array.to_s
      render :json => {:message => 'Booking is being processed', :address => inserted_address, :taxis => closest_taxis_array}, :status => :created
    elsif params[:latitude] && params[:longitude] && params[:latitude]!= 0 && params[:longitude]!= 0 &&
        params[:latitude] != "" && params[:longitude] != ""
      #puts "MA SIIN JOU"
      #Rails.logger.debug "AYY LMAO: " + params[:latitude].to_s + params[:longitude].to_s
      #gets the address from google maps API based on the provided coordinates
      geocoder_response = GeoCoder.get_address(params[:latitude], params[:longitude])
      #just in case between begin and end because some coordinates just don't cut it
      begin
        address = geocoder_response['results'].first['formatted_address']
      end
      #puts "AADRESS: " + address.to_s
      closest_taxis_array = GeoCoder.get_closest_taxi_array(address)
      if closest_taxis_array.any?
        closest_taxi_eta = GeoCoder.get_taxi_eta(closest_taxis_array[0], address)
      end
      #taxi_message = closest_taxis_array[0].username + ' is coming for you, ETA: ' + closest_taxi_eta
      render :json => {:message => 'Booking is being processed', :address => address, :taxis => closest_taxis_array}, :status => :created
    else
      render :json => {:message => 'Now you messed up'}, :status => 400
    end

    # Not working with coordinates yet
    # if cookies[:username] && cookies[:username] == closest_taxi.username
    # if cookies[:username]
    #   Pusher.trigger(closest_taxis_array[0].username.to_s, 'async_notification', {
    #       message: "Someone needs your service!",
    #       client_name: params[:name],
    #       client_number: params[:phone_number],
    #       client_pickup: params[:address]
    #  })
    # end
  end

  def decision
    #get the driver's username to display it to the client later
    taxi_driver_username = params[:username]
    #puts params.to_a.to_s
    #puts "JAAA JUMALAVAITUUUSfffffffffffffffff"
    #puts taxi_driver_username
    #puts "JAAVAITUUS 2222222"
    #puts params[:client_address]
    #puts "IKKAVEEEL JAAA"
    if params[:decision] == 'accept'
      #TODO send the client the notification about the taxi and the ETA
      #send the driver back the client data as a confirmation and to display it to the driver
      client_address = params[:client_address]
      client_phone = params[:client_phone]
      client_name = params[:client_name]
      render json: {:message => "This is fine",
                     :client_address => client_address,
                     :client_phone => client_phone,
                     :client_name => client_name},
              :status => 200
      #console.log("Calculating ETA...")
      #puts "TAXI USERNAME:" + taxi_driver_username
      #puts "TAXI OBJECT:" + get_taxi_by_username(taxi_driver_username).address
      taxi = get_taxi_by_username(taxi_driver_username)
      puts taxi.to_s
      eta = GeoCoder.get_taxi_eta(get_taxi_by_username(taxi_driver_username), client_address)
      #puts " JAAA JUMAALA ETA JAA: " + eta.to_s
      # console.log(eta)
      #send a push notification to the client with 'accepted' message
      #also pack along the name of the driver to display to the client
      Pusher.trigger(params[:client_username].to_s, 'async_notification', {
          message: 'accepted',
          taxi_name: taxi_driver_username,
          eta: eta
      })
      # render :json => {
      #     message: 'accepted',
      #     taxi_name: taxi_driver_username,
      #     eta: eta
      # }, :status => 200
      # puts "OLEN SIIN"

    elsif params[:decision] == 'decline'
      #TODO send the client the notification about that taxi declined
      # OR (easier) change the taxi availability to not available and
      # query the database again for the closest driver and prompt the other driver
      Pusher.trigger(params[:client_username].to_s, 'async_notification', {
          message: 'pick_next', taxi_name: taxi_driver_username
      })

      puts "OLEN SEALLLLL"

      render :json => {:message => 'This is fine'}, :status => 200

    else
      # no decision is applied (shouldn't usually happen)
      render :json => {:message => 'Now you messed up'}, :status => 400
    end
  end


  def make_available
    user_name = params[:taxi].to_s
    #puts Taxi.where(:username => user_name).to_a.to_s
    Taxi.find_by(username: user_name).update(status: 'available')
    #puts Taxi.where(:username => user_name).to_a.to_s
  end

  def make_busy
    user_name = params[:taxi].to_s
    #puts Taxi.where(:username => user_name).to_a.to_s
    Taxi.find_by(username: user_name).update(status: 'busy')
    #puts Taxi.where(:username => user_name).to_a.to_s
  end

  def make_invisible
    user_name = params[:taxi].to_s
    #puts Taxi.where(:username => user_name).to_a.to_s
    Taxi.find_by(username: user_name).update(status: 'invisible')
    #puts Taxi.where(:username => user_name).to_a.to_s
  end

  def ask_driver
    client_address = params[:client_address]
    client_name = params[:name]
    client_number = params[:phone_number]
    client_username = params[:client_username]
    # probably later useful to only send the notification to one driver
    taksi_to_ask = params[:ask_taxi]
    Pusher.trigger(taksi_to_ask.to_s, 'rocky_road', {
        message: "A client needs your service!",
        client_name: client_name,
        client_number: client_number,
        client_pickup: client_address,
        client_username: client_username
    })
  end

  def get_taxi_by_username (username)
    Taxi.find_by(username: username)
  end

  def notify_client_that_ride_started
    Pusher.trigger(params[:client_username].to_s, 'async_notification', {
        message: 'ride_started'
    })
  end
  def notify_client_that_ride_ended
    Pusher.trigger(params[:client_username].to_s, 'async_notification', {
        message: 'ride_ended',
        price: params[:price]
    })
  end
  def notify_taxi_that_client_cancelled
    Pusher.trigger(params[:ask_taxi].to_s, 'rocky_road', {
        message: 'client_cancelled_request',
    })
  end

  def get_user_data
    client_entity = Client.find_by(username: params[:client_username])
    client_phone = client_entity.phone_nr.to_s
    render :json => {:client_phone => client_phone}, :status => 200
  end

end
