class LoginController < ApplicationController

  def login
    #Client = Client.where("username" = :params['username'] && "password" = :params[password])
    if params[:type] == "client"
      if Client.exists?(username: params[:username], password: params[:password])
        session[:username] = params[:username]
        client = Client.where(username: params[:username], password: params[:password]).take
        client.session = session[:session_id]
        client.save
        render :json => {:authenticated => true, :authKey => client.session, :username => params[:username], :type => "client"}
      else
        render :json => {:authenticated => false}
      end
    elsif params[:type] == "taxi"
      if Taxi.exists?(username: params[:username], password: params[:password])
        session[:username] = params[:username]
        taxi = Taxi.where(username: params[:username], password: params[:password]).take
        taxi.session = session[:session_id]
        taxi.save
        render :json => {:authenticated => true, :authKey => taxi.session, :username => params[:username], :type => "taxi"}
      else
        render :json => {:authenticated => false}
      end
    else
      render :json => {:authenticated => false}
    end
  end
  def checkAuth
    sentSession = -1
    session = 0
    if params.has_key?(:type) && params.has_key?(:key) && params.has_key?(:username)
      if params[:type] == "client" then
        if Client.exists?(username: params[:username])
          client = Client.find_by username: params[:username]
          sentSession = params[:key]
          session = client.session
        end
      elsif params[:type] == "taxi" then
        if Taxi.exists?(username: params[:username])
          taxi = Taxi.find_by username: params[:username]
          sentSession = params[:key]
          session = taxi.session
        end
      end
    end
    if sentSession == session
      render :json => {:authenticated => true, :sentSession => sentSession, :session => session}
    else
      render :json => {:authenticated => false, :sentSession => sentSession, :session => session}
    end
  end
  def logout
    if params.has_key?(:type) && params.has_key?(:key) && params.has_key?(:username)
      if params[:type] == "client" then
        if Client.exists?(username: params[:username])
          client = Client.find_by username: params[:username]
          sentSession = params[:key]
          session = client.session
          if (sentSession == session)
            client.session = ""
            client.save
            render :json => {:authenticated => false}
          else
            render :json => {:authenticated => true}
          end
        end
      elsif params[:type] == "taxi" then
        if Taxi.exists?(username: params[:username])
          taxi = Taxi.find_by username: params[:username]
          sentSession = params[:key]
          session = taxi.session
          if (sentSession == session)
            taxi.session = ""
            taxi.save
            render :json => {:authenticated => false}
          else
            render :json => {:authenticated => true}
          end
        end
      end
    end
  end
end
