# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
more_taxis = [
  {:username => 'taxi1', :password => 'parool1', :phone_nr => '+372372' , :status => 'available' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Kalevi 8 51010 Tartu' , :available_since => '25-Nov-1992'},
  {:username => 'taxi2', :password => 'parool2', :phone_nr => '+372872' , :status => 'available' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Ahtri 2 10151 Tallinn' , :available_since => '25-Nov-1992'}
]

more_clients = [
  {:username => 'client1', :password => 'parool1', :phone_nr => '+372372' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Kalevi 8 51010 Tartu'},
  {:username => 'client2', :password => 'parool2', :phone_nr => '+372872' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Järveotsa tee 2 Tallinn'}
]

more_taxis.each do |taxi|
  Taxi.create!(taxi)
end

more_clients.each do |client|
  Client.create!(client)
end
