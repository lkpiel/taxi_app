Given /^I am logged in as ([^"]*)$/ do |driver|
  # step %{I am on the login page}
  visit "http://localhost:8101/#/login"
  @driver = Taxi.find_by_username "#{driver}"
  fill_in('Username', :with => @driver.username)
  fill_in('Password', :with => @driver.password)
  step %{I press "Log in"}
  click_button('Log in')
end

And(/^my status is ([^"]*)$/) do |status|
  @driver.status = status
end

When(/^a customer requests a ride$/) do
  pending
end

Then(/^my status should be ([^"]*)$/) do |status|
  expect(@driver.status).to eq(status)
end

