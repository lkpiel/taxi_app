Then /^I should see a notification$/ do
  page.should have_content("Someone needs your service!")
  page.should have_content("I'm on it!")
  page.should have_content("NNNOPE")
end
