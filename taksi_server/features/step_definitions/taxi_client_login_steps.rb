Given(/^I am not authenticated$/) do
  visit "http://localhost:8100/#/login"
  page.driver.clear_cookies
end

When(/^I provide my client credentials$/) do
  fill_in 'username', with: 'client1'
  fill_in 'password', with: 'parool1'
end

When(/^I provide my taxi credentials$/) do
  fill_in 'username', with: 'taxi1'
  fill_in 'password', with: 'parool1'
end

When(/^I provide false credentials$/) do
  fill_in 'username', with: 'pkspks'
  fill_in 'password', with: 'paul'
end

When(/^I sign in$/) do
    click_button("Log in")
end
When(/^I sign out$/) do
    click_button("Log out")
end
