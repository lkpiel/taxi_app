Given(/^I am in the booking page$/) do
  visit "http://localhost:8100/#/bookings"
end

Given(/^I am in the new booking page$/) do
  visit "http://localhost:8100/#/bookings/new"
end

When(/^I provide my coordinates$/) do
  fill_in 'lat-input', with: '58.375434'
  fill_in 'lng-input', with: '26.728048' end

# When(/^I submit this information$/) do
#   click_on 'submit-coord'
# end

# Then(/^I should be notified that my information is being processed$/) do
#   page.should have_content("Booking is being processed")
# end

Then(/^I should eventually receive a synchronous message with my address$/) do
  page.should have_content("Kalevi 8, 51010 Tartu, Estonia")
end
