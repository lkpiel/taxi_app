When(/^I enter my name$/) do
  fill_in 'name-input', with: 'Peeter'
end

When(/^I enter my phone number$/) do
  fill_in 'phone-input', with: '54265784'
end

When(/^I enter an address$/) do
  fill_in 'address-input', with: 'Juhan Liivi 2, 50409 Tartu'
end

When(/^I submit this information$/) do
  click_on 'submit-coord'
end

Then(/^I should be notified that my information is being processed$/) do
  page.should have_content("Booking is being processed")
end

Then(/^I should eventually receive a message saying where the taxi is coming from$/) do
  page.should have_content("")
end

Then(/^I should eventually receive a message with ETA$/) do
  pending # Write code here that turns the phrase above into concrete actions
end
