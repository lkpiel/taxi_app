Given(/^I am at Liivi 2$/) do
  fill_in 'address-input', with: 'Liivi 2 Tartu'
end

Given(/^there are two taxi available, one next to Kalevi 8 and the other at Lounakeskus$/) do
  #seeds the database before testing
  #load File.join(Rails.root, 'features', 'support', 'seeds_taxi_selection_1.rb')
end

When(/^I submit a booking request$/) do
  click_button 'submit-coord'
end

Then(/^I should receive a confirmation with the taxi next to Kalevi 8$/) do
  page.should have_content("taxi1 is coming for you")
end

Then(/^I should receive a delay estimate$/) do
  page.should have_content("ETA: 5 mins")
end

Given(/^there are two taxi available, at the same distance with respect to Liivi$/) do
  load File.join(Rails.root, 'features', 'support', 'seeds_taxi_selection_2.rb')
end

Then(/^I should receive a confirmation with the taxi that has been available the longest$/) do
  page.should have_content("taxi1 is coming for you")
end
