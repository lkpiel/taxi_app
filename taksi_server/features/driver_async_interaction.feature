Feature: Notification about someone wanting a ride
  As a driver
  So that I can offer a ride
  I want to get notifications with accept and reject buttons

  @javascript
  Scenario: Accepting a request

    Given I am logged in as taxi1
    And my status is available
    When a customer requests a ride
    Then I should see a notification
#    And I should see customer's name and address
    When I press "I'm on it!"
    Then my status should be busy


#    This fails when first one is not commented out...?
#  @javascript
#  Scenario: Rejecting a request
#
#    Given I am logged in as taxi1
#    And my status is available
#    When a customer requests a ride
#    Then I should see a notification
#    And I should see customer's name and address
#    When I press "NNNOPE"
#    Then my status should be available
