Feature: Notification about successful booking
  As a customer
  So that I know when to be ready
  I want to get a notification with ETA (estimated time of arrival)

  @javascript
  Scenario: Asynchronous callback

    Given I am on the booking page
    When I fill in the following:
       | Name         | Phteven    |
       | Phone Number | 10         |
       | Latitude     | 58.3881038 |
       | Longitude    | 26.7030461 |
       | Address      |            |
    And I press "Submit booking request!"
    When The taxi driver accepts the booking
    Then I should receive a push notification
    And I should see "ETA"
