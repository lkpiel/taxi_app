Feature: Taxi driver login
    As a taxi driver
    So that I can access the system
    I want to log in

    @javascript
    Scenario: Logging in (correct credentials)
        Given I am not authenticated
        And I am on the login page
        When I provide my taxi credentials
        And I sign in
        Then I should see "My orders"

    @javascript
    Scenario: Unauthorized login
      Given I am not authenticated
      And I am on the login page
      When I provide false credentials
      And I sign in
      Then I should see "TAXI MAN"

    @javascript
    Scenario: Logging out
      Given I am not authenticated
      And I am on the login page
      When I provide my taxi credentials
      And I sign in
      And I sign out
      Then I should see "TAXI MAN"
