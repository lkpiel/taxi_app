# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
more_taxis = [
    {:username => 'taxi3', :password => 'parool', :phone_nr => '+372372' ,
     :status => 'available' , :latitude => '58.375433' ,
     :longitude => '26.728048' , :address => 'Riia 1 Tartu' ,
     :available_since => '25-Nov-2000'},
    {:username => 'taxi4', :password => 'parool', :phone_nr => '+372372' ,
     :status => 'available' , :latitude => '58.375433' ,
     :longitude => '26.728048' , :address => 'Riia 1 Tartu' ,
     :available_since => '25-Nov-1991'}

]

more_taxis.each do |taxi|
  Taxi.create!(taxi)
end
