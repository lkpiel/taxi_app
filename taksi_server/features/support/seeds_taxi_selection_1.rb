# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#



more_taxis = [
    {:username => 'taxi1', :password => 'parool', :phone_nr => '+372372' ,
     :status => 'available' , :latitude => '58.375433' ,
     :longitude => '26.728048' , :address => 'Riia 1 Tartu' ,
     :available_since => '25-Nov-1992'},
    {:username => 'taxi2', :password => 'parool', :phone_nr => '+372872' ,
     :status => 'available' , :latitude => '58.375433' ,
     :longitude => '26.728048' , :address => 'Ringtee 75 Tartu' ,
     :available_since => '25-Nov-1992'}
]

more_taxis.each do |taxi|
  Taxi.create!(taxi)
end
