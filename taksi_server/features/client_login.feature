Feature: Client login
    As a client
    So that I can order a taxi
    I want to log in

    @javascript
    Scenario: Logging in (correct credentials)
        Given I am not authenticated
        And I am on the login page
        When I provide my client credentials
        And I sign in
        Then I should see "New booking"

    @javascript
    Scenario: Unauthorized login
      Given I am not authenticated
      And I am on the login page
      When I provide false credentials
      And I sign in
      Then I should see "TAXI MAN"


    @javascript
    Scenario: Logging out
      Given I am not authenticated
      And I am on the login page
      When I provide my client credentials
      And I sign in
      And I sign out
      Then I should see "TAXI MAN"
