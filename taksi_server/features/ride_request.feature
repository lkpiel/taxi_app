Feature: Ride request
  As a customer
  So that I can request a ride
  I want to send my name, phone number and address

  @javascript
  Scenario: requesting a ride by entering an address
    Given I am not authenticated
    And I am on the login page
    When I provide my client credentials
    And I sign in
    When I enter my name
    And I enter my phone number
    And I enter an address
    And I submit this information
    Then I should be notified that my information is being processed
    Then I should eventually receive a message with ETA
