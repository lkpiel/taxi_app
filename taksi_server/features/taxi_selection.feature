Feature: Taxi selection
  As a customer
  So that I can have a taxi booking confirmed
  I want one taxi to be selected

  Background:
    Given I am not authenticated
    Given I provide my client credentials
    Given I sign in


  @javascript
  Scenario: Closest taxi
    Given I am in the new booking page
    Given I am at Liivi 2
    Given there are two taxi available, one next to Kalevi 8 and the other at Lounakeskus
    When I submit a booking request
    Then I should receive a confirmation with the taxi next to Kalevi 8
    And I should receive a delay estimate

  @javascript
  Scenario: Multiple candidate taxis
    Given there are two taxi available, at the same distance with respect to Liivi
    When I submit a booking request
    Then I should receive a confirmation with the taxi that has been available the longest
    And I should receive a delay estimate
