require 'spec_helper'
require 'rails_helper'

describe LoginController, type: :controller do
    before(:each) do
      @client = Client.create!(username: 'zimzalabim', password: 'parool1', phone_nr: '+372525252',
          latitude: '', longitude: '', address: "Tammsaare tee")
      @taxi = Taxi.create!(username: 'zulemani', password: 'parool1', phone_nr: '+372525252',
          status: 'available',  latitude: '', longitude: '', address: "Tammsaare tee" , available_since: "")
    end

    it "Responses to login with false client credentials" do
      params = {
        username: "pskpsk",
        password: "pzzt",
        type:     "client"
      }
      post :login, params
      expect(response).to be_success
    end

    it "Responses to login with false taxi credentials" do
      params = {
        username: "pskpsk",
        password: "pzzt",
        type:     "taxi"
      }
      post :login, params
      expect(response).to be_success
    end


    it "Responses to login with correct client credentials" do
      params = {
        username: "zimzalabim",
        password: "parool1",
        type:     "client"
      }
      post :login, params
      expect(response).to be_success
    end

    it "Responses to login with correct taxi credentials" do
      params = {
        username: "zulemani",
        password: "parool1",
        type:     "taxi"
      }
      post :login, params
      expect(response).to be_success
    end

    it "Login with false client credentials should respond false" do
      params = {
        username: "pskpsk",
        password: "pzzt",
        type:     "client"
      }
      post :login, params
      parsed_body = JSON.parse(response.body)
      parsed_body["authenticated"].should == false
    end

    it "Login with false taxi credentials should respond false" do
      params = {
        username: "pskpsk",
        password: "pzzt",
        type:     "taxi"
      }
      post :login, params
      parsed_body = JSON.parse(response.body)
      parsed_body["authenticated"].should == false
    end

    it "Login with correct client credentials should respond with key, username and true" do
      params = {
        username: "zimzalabim",
        password: "parool1",
        type:     "client"

      }
      post :login, params
      parsed_body = JSON.parse(response.body)
      expect(parsed_body.keys).to contain_exactly('authenticated', 'authKey', 'username', 'type')
      expect(parsed_body["authenticated"]).to  eq(true)
      expect(parsed_body["username"]).to eq("zimzalabim")
      expect(session[:username]).to eq("zimzalabim")
    end

    it "Login with correct taxi credentials should respond with key, username and true" do
      params = {
        username: "zulemani",
        password: "parool1",
        type:     "taxi"

      }
      post :login, params
      parsed_body = JSON.parse(response.body)
      expect(parsed_body.keys).to contain_exactly('authenticated', 'authKey', 'username', 'type')
      expect(parsed_body["authenticated"]).to  eq(true)
      expect(parsed_body["username"]).to eq("zulemani")
      expect(session[:username]).to eq("zulemani")
    end

    it "Updates the according database client session field after correct login" do
      params = {
        username: "zimzalabim",
        password: "parool1",
        type:     "client"
      }
      session[:session_id] = "1234"
      post :login, params
      parsed_body = JSON.parse(response.body)
      expect(@client.reload.session).to eq("1234")
    end

    it "Updates the according database taxi session field after correct login" do
      params = {
        username: "zulemani",
        password: "parool1",
        type:     "taxi"
      }
      session[:session_id] = "1234"
      post :login, params
      parsed_body = JSON.parse(response.body)
      expect(@taxi.reload.session).to eq("1234")
    end
    it "After incorrect logout doesn't change session key and returns correctly" do
      params = {
        username: "zimzalabim",
        password: "parool1",
        type:     "client"
      }
      session[:session_id] = "1234"
      post :login, params
      params = {
        username: "zimzalabim",
        key: "1234567",
        type: "client"
      }
      post :logout, params
      parsed_body = JSON.parse(response.body)
      expect(@client.reload.session).to eq("1234")
      expect(parsed_body.keys).to contain_exactly('authenticated')
      expect(parsed_body["authenticated"]).to  eq(true)
    end
    it "After incorrect logout doesn't change session key and returns correctly" do
      params = {
        username: "zulemani",
        password: "parool1",
        type:     "taxi"
      }
      session[:session_id] = "1234"
      post :login, params
      params = {
        username: "zulemani",
        key: "1234567",
        type: "taxi"
      }
      post :logout, params
      parsed_body = JSON.parse(response.body)
      expect(@taxi.reload.session).to eq("1234")
      expect(parsed_body.keys).to contain_exactly('authenticated')
      expect(parsed_body["authenticated"]).to  eq(true)
    end
    it "Updates the according database client session field after correct logout and returns correctly" do
      params = {
        username: "zimzalabim",
        password: "parool1",
        type:     "client"
      }
      session[:session_id] = "1234"
      post :login, params
      params = {
        username: "zimzalabim",
        key: "1234",
        type: "client"
      }
      post :logout, params
      parsed_body = JSON.parse(response.body)
      expect(@client.reload.session).to eq("")
      expect(parsed_body.keys).to contain_exactly('authenticated')
      expect(parsed_body["authenticated"]).to  eq(false)
    end

    it "Updates the according database taxi session field after correct logout and returns correctly" do
      params = {
        username: "zulemani",
        password: "parool1",
        type:     "taxi"
      }
      session[:session_id] = "1234"
      post :login, params
      params = {
        username: "zulemani",
        key: "1234",
        type: "taxi"
      }
      post :logout, params
      parsed_body = JSON.parse(response.body)
      expect(@taxi.reload.session).to eq("")
      expect(parsed_body.keys).to contain_exactly('authenticated')
      expect(parsed_body["authenticated"]).to  eq(false)
    end

    it "checkAuth returns true if client is authenticated" do
      params = {
        username: "zimzalabim",
        password: "parool1",
        type:     "client"
      }
      session[:session_id] = "1234"
      @client.reload.session
      post :login, params

      params = {
       username: "zimzalabim",
       key: "1234",
       type: "client"
      }
      post :checkAuth, params
      parsed_body = JSON.parse(response.body)
      expect(parsed_body["authenticated"]).to  eq(true)
    end

    it "checkAuth returns true if taxi is authenticated" do
      params = {
        username: "zulemani",
        password: "parool1",
        type: "taxi"
      }
      session[:session_id] = "1234"
      @taxi.reload.session
      post :login, params

      params = {
       username: "zulemani",
       key: "1234",
       type: "taxi"
      }
      post :checkAuth, params
      parsed_body = JSON.parse(response.body)
      expect(parsed_body["authenticated"]).to  eq(true)
    end

    it "checkAuth returns false if client is not authenticated" do
      params = {
       username: "zimzalabim",
       key: "1234",
       type: "client"
      }
      post :checkAuth, params
      parsed_body = JSON.parse(response.body)
      expect(parsed_body["authenticated"]).to  eq(false)
    end

    it "checkAuth returns false if user is not authenticated" do
      params = {
       username: "zulemani",
       key: "1234",
       type: "taxi"
      }
      post :checkAuth, params
      parsed_body = JSON.parse(response.body)
      expect(parsed_body["authenticated"]).to  eq(false)
    end

end
