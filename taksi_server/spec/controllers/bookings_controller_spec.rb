require 'spec_helper'
require 'rails_helper'


describe BookingsController, type: :controller do

    before (:each) do
      @taxi1 = Taxi.create!(:username => 'taxi1', :password => 'parool1', :phone_nr => '+372372' , :status => 'available' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Kalevi 8 51010 Tartu' , :available_since => '25-Nov-1992')
      @taxi2 = Taxi.create!(:username => 'taxi2', :password => 'parool2', :phone_nr => '+372872' , :status => 'available' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Ahtri 2 10151 Tallinn' , :available_since => '25-Nov-1992')
      @taxi3 = Taxi.create!(:username => 'taxi3', :password => 'parool3', :phone_nr => '+372872' , :status => 'busy' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Ahtri 2 10151 Tallinn' , :available_since => '25-Nov-1992')
      @client1 = Client.create!(:username => 'client1', :password => 'parool1', :phone_nr => '+372372' , :latitude => '58.375433' , :longitude => '26.728048' , :address => 'Kalevi 8 51010 Tartu')
    end

    context "POST create" do

        it 'sends a message back with address provided' do
            post :create, address: 'Kalevi 8 Tartu'
            expect(response).to have_http_status(:success) # 200
        end

        it 'fails with nothing provided' do
            post :create
            expect(response).not_to be_success
        end

        it 'succeeds with coordinates provided' do
          post :create, latitude: '58.3857335', longitude: '26.6984343', address: 'Kalevi 10 Tartu'
          expect(response).to have_http_status(:success) # 200
        end

        it 'succeeds with coordinates provided with no address' do
          post :create, latitude: '58.3857335', longitude: '26.6984343', address: ''
          expect(response).to have_http_status(:success) # 200
        end

        it 'fails with nothing provided' do
            post :create
            expect(response).not_to be_success
        end

        it 'sends back an array with taxis in them' do
            post :create, address: 'Kalevi 8 Tartu'
            parsed_body = JSON.parse(response.body)
            parsed_body['taxis'].should be_a_kind_of(Array)
        end
    end


    context "POST decision" do

        #before { post :create, :post => {:address => "Kalevi 8 Tartu"} }

        it 'succeeds when send an acceptance decision' do
            post :decision, decision: 'accept', username: 'taxi1', client_address: 'Kalevi 8 Tartu'
            expect(response).to have_http_status(:success) # 200
        end

        it 'succeeds when sending a decline decision' do
            post :decision, decision: 'decline'
            expect(response).to have_http_status(:success) # 200
        end

        it 'fails with nothing provided' do
            post :decision
            expect(response).not_to have_http_status(:success) # 200
        end

        it 'messages the driver of his positive decision' do
            params = {
                client_address: 'Kalevi 8 Tartu',
                client_phone: '1234',
                client_name: 'VAIRA VIKE-FREIBERGA',
                username: 'taxi1',
                decision: 'accept'
            }
            post :decision, params
          expect(response).to have_http_status(:success) # 200
        end

        it 'confirms the clients data' do
            params = {
                client_address: 'Kalevi 8 Tartu',
                client_phone: '1234',
                client_name: 'VAIRA VIKE-FREIBERGA',
                username: 'taxi1',
                decision: 'accept'
            }
            post :decision, params
            expect(response).to have_http_status(:success) # 200
            parsed_body = JSON.parse(response.body)
            parsed_body['client_address'].should eq('Kalevi 8 Tartu')
            parsed_body['client_phone'].should eq('1234')
            parsed_body['client_name'].should eq('VAIRA VIKE-FREIBERGA')
        end

    end


  context 'Database modifying methods' do

    it 'changes driver status from anything to available' do
        expect{ post :make_available, taxi:'taxi3' }.to change { Taxi.find_by(username: 'taxi3').status }.to eq 'available'
    end

    it 'changes driver status from anything to busy' do
      expect{ post :make_busy, taxi:'taxi2' }.to change { Taxi.find_by(username: 'taxi2').status }.to eq 'busy'
    end

    it 'changes driver status from anything to invisible' do
      expect{ post :make_invisible, taxi:'taxi2' }.to change { Taxi.find_by(username: 'taxi2').status }.to eq 'invisible'
    end
  end


  context 'ask driver' do
    it 'sends question to driver with data of the client' do
       post :ask_driver, name: 'Mardo', client_address: 'Kalevi 8 Tartu', phone_number: '1234', client_username: 'client1', ask_taksi: 'taxi1'
       expect(Pusher).to receive(:trigger)
       #true.should == true
    end
  end


  context 'get user phone' do
    it 'sends back user name' do
      post :get_user_data, client_username: 'client1'
      parsed_body = JSON.parse(response.body)
      parsed_body['client_phone'].should eq('+372372')
    end
  end








end
