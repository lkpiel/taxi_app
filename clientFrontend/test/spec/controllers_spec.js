var google = {
    maps : {
        OverlayView : function () {},
        Marker : function () {},
        InfoWindow : function () {},
        MapTypeId : { ROADMAP: 0 },
        Map : function(a, b) {}
    }
};

window.navigator.geolocation = {
    getCurrentPosition: function (success, failure) {
        success({
            coords: {
                latitude: 54.0834,
                longitude: 12.1004
            }, timestamp: Date.now()
        });
    }
};

describe('Controllers.BookingsCtrl', function(){
    var scope, $httpBackend, fnavigator;
    var PusherServiceMock={
      onMessage:function(){}
    };
    // load the controller's module
    beforeEach(module('starter.controllers', 'ionic', 'starter.services'));

    beforeEach(inject(function($rootScope, $controller, $q, _$httpBackend_ ,  _Framework_) {
        $httpBackend = _$httpBackend_;
        scope = $rootScope.$new();
        var _$ionicModal_ = jasmine.createSpyObj('$ionicModal', ['fromTemplateUrl']);
        var modalMock = jasmine.createSpyObj('modal', ['show', 'hide']);
        _$ionicModal_.fromTemplateUrl.and.callFake(function(){
           return $q.when(modalMock);
        });

        fnavigator = $q.defer();
        spyOn(_Framework_, 'navigator').and.returnValue(fnavigator.promise);

        $controller('BookingsCtrl', {$scope: scope, $ionicModal: _$ionicModal_, PusherService: PusherServiceMock, Framework: _Framework_ });
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    // tests start here
    it('should submit a request to the backend service', function () {

      fnavigator.resolve(window.navigator);

      scope.name = "Tom"
      scope.phone_number = "5555555"
      scope.longitude = "58.37829920000001"
      scope.latitude = "26.7146677"
      scope.address = "Liivi 2 Tartu"
      scope.taxis = ['taxi1']
      $httpBackend
        .expectPOST('http://localhost:3000/bookings/new',
          {name: scope.name, phone_number: scope.phone_number, longitude: scope.longitude, latitude: scope.latitude, address: scope.address})
        .respond(200, {message: 'Booking is being processed', address: scope.address, taxis: scope.taxis});
      $httpBackend
        .expectPOST('http://localhost:3000/bookings/ask_driver',
          {client_address: scope.address, name: scope.name, phone_number: scope.phone_number, client_username: 'client1'})
        .respond();
      scope.submit();
      $httpBackend.flush();
    });
});
