var app = angular.module('starter.controllers', ['ngCookies']);

app.controller('BookingsCtrl', function($scope, $ionicModal, $http, PusherService, $cookies, Framework) {
  $('.processingMessage').css('display', 'none');
  $('.clientRiding').css('display', 'none');
  $('.clientMessage').css('display', 'none');
  $('.clientMessage2').css('display', 'none');

  $('.ordersForm').css('display', 'block');
  $scope.name = $cookies.get("username");
  $scope.phone_number = '';
  $scope.longitude = 0;
  $scope.latitude = 0;
  $scope.address = '';
  $scope.taxi_info = '';
  $scope.taxis = [];
  $scope.eta = '';
  var my_address = '';
  var taxi_name = "";

  $http.post('http://localhost:3000/bookings/get_user_data ', {client_username: $cookies.get("username")})
  .then(function (response) {
    $scope.phone_number = response.data.client_phone;
    document.getElementById("clientPhone").value = response.data.client_phone;
  });


  Framework.navigator().then(function (navigator) {
    navigator.geolocation.getCurrentPosition(function(position) {
      console.log("I am inside");
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      var mapProp = {
        center: {lat: latitude, lng: longitude},
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      $scope.map = new google.maps.Map(document.getElementById('map'), mapProp);
      $scope.marker = new google.maps.Marker({ map: $scope.map, position: mapProp.center});

    //  $scope.$apply(function (){
      $scope.latitude = latitude;
      $scope.longitude = longitude;
      document.getElementById("latitude").value = latitude;
      document.getElementById("longitude").value = longitude;
    //  });
      $scope.map.addListener('click', function(event) {
          $scope.marker.setPosition(event.latLng);
          $scope.latitude = event.latLng.lat();
          $scope.longitude = event.latLng.lng();
          $scope.address = '';
          document.getElementById("address").value = '';
          document.getElementById("latitude").value = event.latLng.lat();
          document.getElementById("longitude").value = event.latLng.lng();
      });
      });
  });

  $scope.submit = function() {
    //$scope.modal.show();
    //http://localhost:3000/bookings/new
    //bad request with /new, but this should be right

    $scope.name = document.getElementById('clientName').value;
    $scope.phone_number = document.getElementById('clientPhone').value;
    $scope.address = document.getElementById('address').value;

    $http.post('http://localhost:3000/bookings/new', {name: $scope.name, phone_number: $scope.phone_number, longitude: $scope.longitude, latitude: $scope.latitude, address: $scope.address})
      .then(function (response) {



        $('.ordersForm').css('display', 'none');
        $('.processingMessage').css('display', 'block');
        var current_taxi;
        $scope.taxis = response.data.taxis;
        $scope.taxi_info = response.data.message;
        $scope.ETA = ''
        //$scope.ETA = response.data.address;
        //$scope.modal.show();
        if($scope.taxis.length > 0){
          console.log.current_taxi;
        current_taxi = $scope.taxis[0].username;
        my_address = response.data.address
        //http://localhost:3000/bookings/ask_driver
          $scope.taxis.shift();
          $http.post('http://localhost:3000/bookings/ask_driver', {ask_taxi: current_taxi, client_address: my_address, name: $scope.name, phone_number: $scope.phone_number, client_username: 'client1'})
        } else {
          //$scope.modal.hide();

          $('.ordersForm').css('display', 'block');
          $('.processingMessage').css('display', 'none');
          alert("Could not find a taxi for you! Please try again later, my friend!");
        }
      });
  };

  $scope.searchAddress = function (){
  var geocoder = new google.maps.Geocoder();
  $scope.address = document.getElementById('address').value;
  var address = document.getElementById('address').value;
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == 'OK') {
      $scope.map.setCenter(results[0].geometry.location);
      $scope.marker.setPosition(results[0].geometry.location);
      // alert("address: " + results[0].geometry.location.lat());
      $scope.latitude = results[0].geometry.location.lat();
      $scope.longitude = results[0].geometry.location.lng();
      document.getElementById("latitude").value = results[0].geometry.location.lat();
      document.getElementById("longitude").value = results[0].geometry.location.lng();
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

  $scope.cancelRide = function (){
    $('.processingMessage').css('display', 'none');
    $('.clientRiding').css('display', 'none');
    $('.clientMessage').css('display', 'none');
    $('.clientMessage2').css('display', 'none');

    $('.ordersForm').css('display', 'block');
    $http.post('http://localhost:3000/bookings/notify_taxi_that_client_cancelled', {ask_taxi: taxi_name})
      .then(function (response) {
        console.log("Notifyed driver!");
    });
  }
  $scope.newOrder = function (){
    $('.processingMessage').css('display', 'none');
    $('.clientRiding').css('display', 'none');
    $('.clientMessage').css('display', 'block');

    $('.clientMessage2').css('display', 'none');


    $('.ordersForm').css('display', 'block');
  }

  $scope.asyncNotification = '';
  console.log()
  PusherService.onMessage(function(response) {
    console.log("PUSHER");
    if (response.message == "ride_started"){
      $scope.taxi_info = "Enjoy your ride!";
      $scope.eta = "";
      $('.clientMessage').css('display', 'none');
    } else if (response.message == "ride_ended"){
      $('.clientMessage2').css('display', 'block');
      console.log(response.price + ": PRICE");
      $scope.taxi_info = "This ride cost you:";
      $scope.eta = response.price.toFixed(1) + " €";

    } else {
    var current_taxi;
    // to display it later to the client that 'blabla' is coming for you
    taxi_name = response.taxi_name.toString();
    if (response.message == 'pick_next') {
            // last taxi-man didn't want any business
      console.log("Taxis length" + $scope.taxis.length);
      if($scope.taxis.length != 0){
             // check if we have any taxis left to ask from
        current_taxi = $scope.taxis[0].username;  // come up with a new taxi
        taxi_name = current_taxi;

        $scope.taxis.shift()      // removes first element of the array
        //http://localhost:3000/bookings/ask_driver
        // go ahead and ask another driver to do business
        $http.post('http://localhost:3000/bookings/ask_driver', {ask_taxi: current_taxi, client_address: my_address, name: $scope.name, phone_number: $scope.phone_number, client_username: 'client1'})

      } else {
        //$scope.modal.hide();
        $('.ordersForm').css('display', 'block');
        $('.processingMessage').css('display', 'none');
        alert("Could not find a taxi for you! Please try again later, my dear friend!");
      }

    } else if (response.message == 'accepted'){
      // display the user that this taxi is coming for hom/her

      $scope.taxi_info = taxi_name + " is coming for you";
      $scope.eta = "ETA: " + response.eta.toString();
      //$scope.modal.show();
      $('.processingMessage').css('display', 'none');
      $('.clientRiding').css('display', 'block');
      $('.clientMessage').css('display', 'block');
      $('.clientMessage2').css('display', 'none');

    } else {
      $scope.asyncNotification = response.message;
    }
  }
    // TODO: in backend modify method so that sends to taxi driver with certain username
  });

  /*$ionicModal.fromTemplateUrl('templates/bookings/sync-notification.html', {
    scope: $scope
  }).then(function(modal) {
  //$scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.taxi_info = '';
    //$scope.modal.hide();
  };*/
});
//http://localhost:3000/login
app.controller('LoginCtrl', function($location, $scope, $http, $cookies) {
  $scope.doLogin = function () {
    $http.post('http://localhost:3000/login', {username: $scope.username, password: $scope.password, type: "client"})
      .then(function (response) {
          if (response.data.authenticated == true && response.data.type == "client") {
            $scope.message = "";
            $cookies.put("username", response.data.username)
            $cookies.put("key", response.data.authKey)
            $location.path('/bookings/new');
          }
          else{
            $cookies.remove("username")
            $cookies.remove("key")
            $scope.message = "Invalid username or password"
          }
      });
  };
  //http://localhost:3000/logout
  $scope.doLogout = function (){

    $http.post('http://localhost:3000/logout', {username: $cookies.get("username"), key: $cookies.get("key"), type: "client"})
      .then(function (response) {
          $('.ordersForm').css('display', 'block');
          $('.waiting').css('display', 'none');
          if(response.data.authenticated  == false){
            $cookies.remove("username");
            $cookies.remove("key");
            $location.path('/login');
          } else {
            alert("Unsuccessful logout. Please contact support if problems continue.");
          }
      });
  }
});
