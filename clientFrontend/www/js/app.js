var app = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCookies']);

app.run(function($ionicPlatform, $rootScope, $cookies, $http, $window) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    //http://localhost:3000/checkAuth
    $rootScope.$on( '$stateChangeStart', function(e, toState  , toParams , fromState, fromParams) {
      $http.post('http://localhost:3000/checkAuth', {username: $cookies.get("username"), key: $cookies.get("key"), type:"client"})
        .then(function (response) {
            if (response.data.authenticated == false){
              $window.location.href  = "./#/login"
            }
        });
    });
    //http://localhost:3000/checkAuth
    $http.post('http://localhost:3000/checkAuth', {username: $cookies.get("username"), key: $cookies.get("key"), type:"client"})
      .then(function (response) {
          if (response.data.authenticated == false)
            $window.location.href  = "./#/login"
    });
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('bookings', {
      url: '/bookings',
      abstract:true,
      templateUrl: 'templates/menu.html'
    })
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })
    .state('bookings.new', {
      url: '/new',
      views: {
        'menuContent': {
          templateUrl: 'templates/bookings/new.html'
        }
      },
      controller: 'BookingsCtrl'
    });
    $urlRouterProvider.otherwise('/login');
});
