'use strict';

var app = angular.module('starter.services', []);

app.service('PusherService', function ($rootScope, $cookies) {
  var pusher = new Pusher('9a112866bc5824b24c9c');
  var channel = pusher.subscribe($cookies.get("username"));
  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});

app.factory('Framework', function ($q) {
  var _navigator = $q.defer();
  var _cordova = $q.defer();
  console.log('I am in');

  if (window.cordova === undefined) {
    _navigator.resolve(window.navigator);
    _cordova.resolve(false);
  } else {
    document.addEventListener('deviceready', function (evt) {
      _navigator.resolve(navigator);
      _cordova.resolve(true);
    });
  }

  console.log('I am in');

  return {
    navigator: function() { return _navigator.promise; },
    cordova: function() { return _cordova.promise; }
  };
});
